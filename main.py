# Purpose: Create a simple static web calendar for EMPUG
# See https://github.com/sukeesh/flask-calendar
# also https://fullcalendar.io/
#
# 2019 03 25 AJL Created file from GitHub repo
#
from flask import Flask, request, render_template, jsonify
import json

app = Flask(__name__)


@app.route('/webcalendar')
def calendar():
    return render_template("empug.html")


@app.route('/data')
def return_data():
    start_date = request.args.get('start', '')
    end_date = request.args.get('end', '')

    with open("events.json", "r") as input_data:
        return input_data.read()

if __name__ == '__main__':
    app.debug = True
    app.run(debug=True, host='0.0.0.0')
