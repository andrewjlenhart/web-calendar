# EMPUG Web Site Calendar

## Sources
https://github.com/sukeesh/flask-calendar and 
https://fullcalendar.io/

## Notes
* Minor updates to customize for EMPUG web site
* Events are stored in events.json as JSON strings
* See fullcalendar.io for CSS details to modify look and feel of the calendar
* The calendar cannot be edited on-line
